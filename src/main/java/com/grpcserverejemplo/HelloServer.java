package com.grpcserverejemplo;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class HelloServer {
    public static void main(String[] args) {
        Server server = ServerBuilder
                .forPort(6565)
                .addService(new HelloServiceImpl())
                .build();
        try {
            server.start();
            System.out.println("Server started");
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }
}
